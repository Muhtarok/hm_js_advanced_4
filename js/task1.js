"use script";

console.log("THEORY");

console.log("1. AJAX - формат рішення, коли вебсайт надсилає запит на сервер, отримує звідти інформацію і якимось чином додає його на сторінку без перезавнтаження сторінки, використовуючи XML");

console.log("PRACTICE");

// Технічні вимоги:

/* Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. 
Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. 
Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.


Необов'язкове завдання підвищеної складності

Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
 */
console.log("TASK 1");

const mainURL = "https://ajax.test-danit.com/api/swapi/films";


fetch(mainURL)
    .then(res => res.json())
    .then(data => {
        console.log(data);
        const filmListContainer = document.createElement("div");
        document.body.append(filmListContainer);
        data.forEach(({ episodeId, id, name, openingCrawl, characters }) => {

            console.log(characters);


            const oneFilmContainer = document.createElement("div");
            // const list = document.createElement("ul");

            filmListContainer.append(oneFilmContainer);
            oneFilmContainer.insertAdjacentHTML("beforeend", `<div>
                <h1 class="">${name}</h1>
                <p> List of characters: </p>
                <p>Episode №${episodeId}</p>
                <p>${openingCrawl}</p>
            </div>`);
            oneFilmContainer.setAttribute("id", `${id}`);

            // const rightArray = data.filter(({ albumId }) => albumId <= 10);
            // console.log(rightArray);

            // rightArray.forEach(({ albumId, title, url }) => {
            //      const container = document.querySelector(`#container_${albumId}`);

            // const charactersArray = [];
            const list = oneFilmContainer.querySelector("p");
            characters.forEach((charactersList, i, arr) => {
                fetch(charactersList)
                    .then(res => res.json())
                    .then((charactersList) => {
                        // console.log(charactersList);
                        // console.log(charactersList.name);


                        // list.insertAdjacentHTML("beforeend", `<li> ${charactersList.name} </li>`);

                        list.textContent+=`${charactersList.name} , `;
                        // charactersArray.push(charactersList.name);



                        // list.insertAdjacentHTML("beforeend", ` ${charactersList.name} `);
                        // oneFilmContainer.append(list);
                        // oneFilmContainer.appendChild(charactersList.name);
                        // oneFilmContainer.insertAdjacentText("beforeend", charactersList.name);
                        // "The list of characters: ", 
                        
                     
                    })
            });
            // console.log(charactersArray);
            
            // console.log(stringCharacters);
            
            // list.insertAdjacentHTML("beforeend", ` ${stringCharacters} `);

        })

    })
    .catch((error) => {
        console.log(error)
    })
